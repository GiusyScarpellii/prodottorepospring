package com.lez21.repos;

import java.util.*;

import org.springframework.stereotype.Repository;

@Repository
public interface DataAccessRepo<T> {
	
	void inserti(T t);
	T findById(int id);
	List<T> findAll();
	boolean delete(int id);
	boolean delete(T t);
	boolean update(T t);
	
}
