package com.lez21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdottoRepoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdottoRepoSpringApplication.class, args);
	}

}
